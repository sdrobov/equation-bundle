<?php


namespace App\Tests\Mock;


use App\Entity\EquationInterface;
use App\Service\EquationResolverInterface;

class EquationResolverServiceMock implements EquationResolverInterface
{
    public const RESULT = 2;

    public function resolve(EquationInterface $equation)
    {
        $equation->setResult(self::RESULT);
    }
}
