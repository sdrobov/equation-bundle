<?php


namespace App\Tests\Mock;


use App\Entity\EquationInterface;
use App\Entity\SimpleEquation;
use App\Service\EquationParserInterface;

class EquationParserServiceMock implements EquationParserInterface
{
    public const DATA = '2';

    /** @var EquationInterface */
    private $equation;

    public function __construct()
    {
        $this->equation = new SimpleEquation();
    }

    public function parse(string $raw): EquationInterface
    {
        $this->equation->setData($raw);

        return $this->equation;
    }
}
