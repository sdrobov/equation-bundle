<?php

namespace App\Tests\Service;

use App\Entity\SimpleEquation;
use App\Service\SimpleEquationParserService;
use App\Tests\Mock\EquationParserServiceMock;
use PHPUnit\Framework\TestCase;

class SimpleEquationParserServiceTest extends TestCase
{
    public function testParseValid()
    {
        $parser = new SimpleEquationParserService(new SimpleEquation());
        $equation = $parser->parse(EquationParserServiceMock::DATA);

        $this->assertEquals(EquationParserServiceMock::DATA, $equation->getData());
    }

    public function testParseInvalid()
    {
        $raw = 'die("Hacked")';
        $this->expectExceptionMessage("'{$raw}' is not valid math equation");

        $parser = new SimpleEquationParserService(new SimpleEquation());
        $parser->parse($raw);
    }
}
