<?php

namespace App\Tests\Service;

use App\Service\EquationFacade;
use App\Tests\Mock\EquationParserServiceMock;
use App\Tests\Mock\EquationResolverServiceMock;
use PHPUnit\Framework\TestCase;

class EquationFacadeTest extends TestCase
{
    public function testResolve()
    {
        $parser = new EquationParserServiceMock();
        $resolver = new EquationResolverServiceMock();
        $facade = new EquationFacade($parser, $resolver);

        $this->assertEquals(EquationResolverServiceMock::RESULT, $facade->resolve(EquationParserServiceMock::DATA));
    }
}
