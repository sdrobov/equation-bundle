<?php

namespace App\Tests\Service;

use App\Entity\SimpleEquation;
use App\Service\SimpleEquationResolverService;
use App\Tests\Mock\EquationParserServiceMock;
use PHPUnit\Framework\TestCase;

class SimpleEquationResolverServiceTest extends TestCase
{
    public function testResolve()
    {
        $equation = new SimpleEquation();
        $equation->setData(EquationParserServiceMock::DATA);

        $resolver = new SimpleEquationResolverService();
        $resolver->resolve($equation);

        $this->assertEquals(EquationParserServiceMock::DATA, $equation->getResult());
    }
}
