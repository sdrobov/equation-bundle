<?php


namespace App\Entity;


class SimpleEquation implements EquationInterface
{
    private $data;
    private $result;

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }
}
