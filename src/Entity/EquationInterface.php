<?php


namespace App\Entity;


interface EquationInterface
{
    public const TAG = 'equation.entity';

    public function getData();

    public function setData($data);

    public function getResult();

    public function setResult($result);
}
