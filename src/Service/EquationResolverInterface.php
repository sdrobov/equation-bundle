<?php


namespace App\Service;


use App\Entity\EquationInterface;

interface EquationResolverInterface
{
    public const TAG = 'equation.resolver';

    public function resolve(EquationInterface $equation);
}
