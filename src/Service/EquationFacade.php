<?php


namespace App\Service;


class EquationFacade
{
    /** @var EquationParserInterface */
    private $parser;

    /** @var EquationResolverInterface */
    private $resolver;

    public function __construct(EquationParserInterface $parser, EquationResolverInterface $resolver)
    {
        $this->parser = $parser;
        $this->resolver = $resolver;
    }

    public function resolve(string $rawEquation)
    {
        $equation = $this->parser->parse($rawEquation);
        $this->resolver->resolve($equation);

        return $equation->getResult();
    }
}
