<?php


namespace App\Service;


use App\Entity\EquationInterface;

class SimpleEquationResolverService implements EquationResolverInterface
{
    public function resolve(EquationInterface $equation)
    {
        $result = 0;
        $data = $equation->getData();

        try {
            eval("\$result = {$data};");
        } catch (\Throwable $e) {
            throw new \RuntimeException("Cant resolve {$data}: " . $e->getMessage(), 10, $e);
        }

        $equation->setResult($result);
    }
}
