<?php


namespace App\Service;


use App\Entity\EquationInterface;

interface EquationParserInterface
{
    public const TAG = 'equation.parser';

    public function parse(string $raw): EquationInterface;
}
