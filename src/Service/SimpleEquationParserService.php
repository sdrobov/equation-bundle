<?php


namespace App\Service;


use App\Entity\EquationInterface;

class SimpleEquationParserService implements EquationParserInterface
{
    /** @var EquationInterface */
    private $equation;

    public function __construct(EquationInterface $equation)
    {
        $this->equation = $equation;
    }

    public function parse(string $raw): EquationInterface
    {
        if (preg_match('/[^\s\d\+\-\/\*\(\)\^]/', $raw)) {
            throw new \InvalidArgumentException("'{$raw}' is not valid math equation");
        }

        $raw = str_replace('^', '**', $raw);

        $this->equation->setData($raw);

        return $this->equation;
    }
}
