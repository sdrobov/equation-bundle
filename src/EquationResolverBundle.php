<?php


namespace App;


use App\Entity\EquationInterface;
use App\Service\EquationParserInterface;
use App\Service\EquationResolverInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EquationResolverBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->registerForAutoconfiguration(EquationInterface::class)
            ->addTag(EquationInterface::TAG);

        $container->registerForAutoconfiguration(EquationParserInterface::class)
            ->addTag(EquationParserInterface::TAG);

        $container->registerForAutoconfiguration(EquationResolverInterface::class)
            ->addTag(EquationResolverInterface::TAG);
    }
}
